<?php


class ImageHandler {
  /**
   * To make sure that memory doesn't become an issue, get each category synchronously.
   *
   * @return void
   */
  public static function init() {
    self::get_providers('doctor');
    self::get_providers('dentist');

    exit('Finished getting providers');
  }
  /**
   * A wrapper around create_image to catch errors.
   * First creates an images directory if it's not already available
   *
   * @param string $image_string - a base64 encoded image
   * @param string $profile_id - the ID of the Dr.
   * @return void
   */
  static public function create_all_images($image_string, $profile_id) {
    if (!file_exists(dirname(__FILE__) . '/provider-images')) {
      mkdir(dirname(__FILE__) . '/provider-images', 0777, true);
    }
    try {
      self::create_image($image_string, $profile_id);
    } catch (Exception $e) {
      return false;
    }
  }

  /**
   * Create an image and name it with the Dr's ID
   *
   * @param string $image_string - a base64 encoded image
   * @param string $profile_id - the ID of the Dr.
   * @return void
   */
  static public function create_image($image_string, $profile_id) {

    if (!$image_string) {
      throw new Exception("Invalid Image String for $profile_id", 1);
    }

    if (!$profile_id) {
      throw new Exception("Invalid Profile Id", 1);
    }

    $file = fopen(dirname(__FILE__) . '/provider-images'. '/' . $profile_id . '.png', 'wb');

    fwrite($file, base64_decode($image_string));

    fclose($file);
  }

  /**
   * Get providers for each specialty from the endpoint
   * specialties are chunked to handle memory/performance
   *
   * @return array
   */
   static public function get_providers($category) {

    $specialties = self::get_specialty_names($category);

    $chunks = array_chunk($specialties, 10);

    // change this back to count($chunks)
    for ($i = 0; $i < count($chunks); $i++) {
      
      foreach ($chunks[$i] as $chunk => $specialty) {
        $url = self::get_base_url() . 'Provider?category=' . strtoupper($category) . '&specialty=' . $specialty;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    
        // Execute request
        $data = curl_exec($ch);
    
    
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        curl_close($ch);
    
        $data = json_decode($data);
    
    
        $data = array_map(function($item) {
          $filtered = array();
    
          $profile_id_exists = property_exists($item, 'ProfileId');
          $profile_id_is_valid = boolval($item->ProfileId);
    
          $photo_exists = property_exists($item, 'Photo');
          $photo_is_valid = boolval($item->Photo);
    
          if ($profile_id_exists && $profile_id_is_valid) {
            $filtered['ProfileId'] = $item->ProfileId;
          } else {
            $filtered['ProfileId'] = null;
            echo "no profile id " . $item->FirstName . ' ' . $item->LastName;
          }
    
          if ($photo_exists && $photo_is_valid) {
            $filtered['Photo'] = $item->Photo;
          } else {
            $filtered['Photo'] = null;
            echo "no photo " . $item->FirstName . ' ' . $item->LastName;
          }

          self::create_all_images($filtered['Photo'], $filtered['ProfileId']);
    
          return true;
        }, $data);

      }

    }

    return $data;

  }

  /**
   * Requests specialties from the API and returns an array of url encoded specialty names
   *
   * @return array
   */
   static public function get_specialty_names($category) {
    $url = self::get_specialties_endpoint($category);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    // Execute request
    $data = curl_exec($ch);
    if (curl_errno($ch)) {
        throw new Exception(curl_error($ch));
    }
    curl_close($ch);

    $data = json_decode($data);

    // prepare to return just the names from the response
    $names = array_map(function($item) {
      return urlencode($item->Name);
    }, $data);

    return $names;
  }

  /**
   * The base API endpoint
   * 
   * @return string
   */
  static public function get_base_url(): string {
    return 'https://publicdirectoryapi.uchc.edu/api/';
  }

  /**
   * Create the endpoint for retrieving doctor specialties
   *
   * @return string
   */
  static public function get_specialties_endpoint($category) {

    return self::get_base_url() . 'Providers/'. $category .'/specialties';
  }

}

$image_handler = new ImageHandler();
$image_handler::init();