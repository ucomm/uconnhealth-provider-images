FROM uconn/php74-official

WORKDIR /project

COPY ./image_handler.php /project/image_handler.php

RUN echo "memory_limit = 512M" >> /usr/local/etc/php/conf.d/memory-limit.ini

ENTRYPOINT ["php", "image_handler.php"]