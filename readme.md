# Find a Provider API image handler

This is a work in progress. The goals are to 

- read providers from the API
- decode the image from base64 to png
- name the image with the provider id
- save the image to a directory

this project is meant to be run on jenkins. it will do the above and then send the files to our CDN.

## Usage

### local
```
# build a docker image from the current Dockerfile and tag it provider-images
docker build -t provider-images .

# run the image as a container with the following attributes
# - --rm -> destroy the container when the task is finished
# - -v -> bind the current directory's provider-images directory to the container
docker run --rm -v "$PWD/provider-images":/project/provider-images provider-images
```

### jenkins
```
# build a docker image from the current Dockerfile and tag it provider-images
docker build -t provider-images .

# run the image as a container with the following attributes
# - --rm -> destroy the container when the task is finished
# - -e -> set a jenkins environment variable ($JOB_URL) inside the container.
#     this tells the php script its running in the jenkins context
# - -v -> bind the workspace provider-images directory to the container
docker run --rm -e JOB_URL=$JOB_URL -v "$WORKSPACE/provider-images":/project/provider-images provider-images
```

## TODO



## Known Issues
- this takes a long time to run. there are a lot of providers!